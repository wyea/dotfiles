#!/bin/bash
echo "The script starts now!"
cd ~
echo "========================================================================"
echo "Updating rbenv..."
rbenv update
echo "========================================================================"
echo "Updating gems..."
gem update --system
gem update
gem cleanup
echo "========================================================================"
echo "Updating oh-my-zsh..."
cd ~/.oh-my-zsh/
git pull && git submodule update --init --recursive
echo "========================================================================"
echo "Updating oh-my-zsh powerlevel9k theme..."
cd ~/.oh-my-zsh/custom/themes/powerlevel9k/
git pull && git submodule update --init --recursive
echo "========================================================================"
cd ~
