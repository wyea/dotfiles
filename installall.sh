#!/bin/bash

# ADD REPOSITORIES {{{
add_repositories(){
  if [ -f /etc/apt/sources.list.d/passenger.list ]
    then
      echo "/etc/apt/sources.list.d/passenger.list exists."
    else
      # Install Passenger PGP key and add HTTPS support for APT
      sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
      sudo apt-get install -y apt-transport-https ca-certificates
      # Add Passenger APT repository
      sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
  fi
  if [ -f /etc/apt/sources.list.d/nginx.list ]
    then
      echo "/etc/apt/sources.list.d/nginx.list exists."
    else
      # Install Nginx PGP key
      wget http://nginx.org/keys/nginx_signing.key
      sudo apt-key add nginx_signing.key
      # Add Nginx APT repository
      sudo sh -c 'echo deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx > /etc/apt/sources.list.d/nginx.list'
      sudo sh -c 'echo deb-src http://nginx.org/packages/mainline/ubuntu/ trusty nginx > /etc/apt/sources.list.d/nginx.list'
      rm nginx_signing.key
  fi
  if [ -f /etc/apt/sources.list.d/pgdg.list ]
    then
      echo "/etc/apt/sources.list.d/pgdg.list exists."
    else
      # Install PostgreSQL key
      wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
      # Add PostgreSQL APT repository
      sudo sh -c 'echo deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main > /etc/apt/sources.list.d/pgdg.list'
  fi
  if [ -f /etc/apt/sources.list.d/mongodb-org-3.2.list ]
    then
      echo "/etc/apt/sources.list.d/mongodb-org-3.2.list exists."
    else
      # Install MongoDB key
      wget --quiet -O - https://www.mongodb.org/static/pgp/server-3.2.asc | sudo apt-key add -
      # Add MongoDB APT repository
      sudo sh -c 'echo deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse > /etc/apt/sources.list.d/mongodb-org-3.2.list'
  fi
  if [ -f /etc/apt/sources.list.d/nodesource.list ]
    then
      echo "/etc/apt/sources.list.d/nodesource.list exists."
    else
      # Install Node.js repository
      curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
  fi
  sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade
}
#}}}

# RUBY ENVIRONMENT {{{
ruby_environment(){
  cd && sudo apt-get update
  sudo apt-get install -y git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev nodejs
  git clone git://github.com/rbenv/rbenv.git ~/.rbenv
  echo '' >> ~/.bashrc
  echo '# Ruby environment (rbenv)' >> ~/.bashrc
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
  echo 'eval "$(rbenv init -)"' >> ~/.bashrc

  git clone git://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
  echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc

  git clone https://github.com/rbenv/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
  git clone https://github.com/rbenv/rbenv-vars.git ~/.rbenv/plugins/rbenv-vars
  git clone https://github.com/rkh/rbenv-update.git ~/.rbenv/plugins/rbenv-update
  ~/.rbenv/bin/rbenv rehash

  ~/.rbenv/bin/rbenv install 2.3.1
  ~/.rbenv/bin/rbenv global 2.3.1
  ~/.rbenv/bin/rbenv rehash
  ruby -v
  echo "gem: --no-ri --no-rdoc" > ~/.gemrc
  ~/.rbenv/shims/gem install bundler
  ~/.rbenv/shims/gem install rails
  ~/.rbenv/bin/rbenv rehash
}
#}}}

# SETUP NGINX AND PASSENGER {{{
nginx_passenger(){
  cd && sudo apt-get update
  sudo apt-get install -y nginx-extras passenger
  sudo sed -i "s%# passenger_root%passenger_root%g" /etc/nginx/nginx.conf
  sudo sed -i "s%# passenger_ruby%passenger_ruby%g" /etc/nginx/nginx.conf
  sudo sed -i "s%/usr/bin/passenger_free_ruby%/home/vagrant/.rbenv/shims/ruby%g" /etc/nginx/nginx.conf
  sudo sed -i "s%listen 80 default_server%# listen 80 default_server%g" /etc/nginx/sites-available/default
  sudo sed -i "s%listen \[\:\:\]\:80 default_server%# listen \[\:\:\]\:80 default_server%g" /etc/nginx/sites-available/default
  sudo touch /etc/nginx/conf.d/sample_app_conf
  echo "server {" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "  listen 80 default_server;" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "  server_name project.dev;" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "  passenger_enabled on;" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "  passenger_app_env development;" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "  root /vagrant/sample_app/public;" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
  echo "}" | sudo tee --append /etc/nginx/conf.d/sample_app_conf > /dev/null
}
#}}}

# SETUP POSTGRESQL {{{
postgresql_setup(){
  cd && sudo apt-get update
  sudo apt-get install -y libpq-dev postgresql postgresql-contrib
  sudo -u postgres createuser vagrant
  sudo -u postgres psql --command "ALTER USER vagrant WITH PASSWORD 'vagrant';"
  sudo -u postgres psql --command "ALTER USER vagrant WITH createdb;"
  ~/.rbenv/shims/gem install pg
  ~/.rbenv/bin/rbenv rehash
}
#}}}

# ZSH AND OH-MY-ZSH SETUP {{{
oh_my_zsh_setup(){
  cd && sudo apt-get update
  sudo apt-get install -y git zsh
  git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
  mkdir ~/.oh-my-zsh/custom/themes
  git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
  cp /vagrant/.zshrc ~/
  chsh -s /bin/zsh
}
#}}}

while true
do
  PS3='Please enter your choice: '
  options=("Add repositories" "Install rbenv" "Setup Nginx and Passenger" "Setup PostgreSQL" "Setup oh-my-zsh" "Quit")
  select opt in "${options[@]}"
  do
      case $opt in
          "Add repositories")
              add_repositories
              break
              ;;
          "Install rbenv")
              ruby_environment
              break
              ;;
          "Setup Nginx and Passenger")
              nginx_passenger
              break
              ;;
          "Setup PostgreSQL")
              postgresql_setup
              break
              ;;
          "Setup oh-my-zsh")
              oh_my_zsh_setup
              break
              ;;
          "Quit")
              echo "Thank You..."
              exit
              ;;
          *) echo invalid option;;
      esac
  done
done
