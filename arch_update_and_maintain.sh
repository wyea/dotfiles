#!/bin/bash
echo "The script starts now!"
cd ~
echo "========================================================================"
echo "Updating list of mirrors..."
sudo reflector --verbose --country 'United States' -p http --sort rate --save /etc/pacman.d/mirrorlist
#sudo reflector --verbose --country 'Australia' -p http --sort rate --save /etc/pacman.d/mirrorlist
echo "========================================================================"
echo "Updating installed packages..."
yay -Syua
echo "========================================================================"
echo "Updating vim plugins..."
vim +PlugUpgrade +PlugUpdate! +qall
echo "========================================================================"
echo "Updating rbenv..."
cd "$(rbenv root)"
git pull
cd "$(rbenv root)"/plugins/ruby-build && git pull
cd ~
echo "========================================================================"
echo "Updating gems..."
gem update --system
gem update
gem cleanup
echo "========================================================================"
echo "Updating oh-my-zsh..."
cd ~/.oh-my-zsh/
git pull && git submodule update --init --recursive
echo "========================================================================"
echo "Updating oh-my-zsh powerlevel9k theme..."
cd ~/.oh-my-zsh/custom/themes/powerlevel9k/
git pull && git submodule update --init --recursive
cd ~
echo "========================================================================"
echo "Updating python packages..."
pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs sudo pip install -U
echo "========================================================================"
echo "Deleting old system logs..."
sudo journalctl --vacuum-size=100M
